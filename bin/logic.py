import copy

import pickle
import os

AND = lambda a, b: bool(a) and bool(b)
OR = lambda a, b: bool(a) or bool(b)
NOT = lambda a: not bool(a)

class outlet:
    def __init__(self, owner, name, out):
        self.value = False
        self.owner = owner
        
        self.type = 'outlet'
        self.name = name

        self.out = out
        
        self.connects = []  
        
        self.level = 0

        self.inputed = False

        self.deleted = None

    def connect(self, inputs):
        if type(inputs) != type([]):
            inputs = [inputs]
        
        for input in inputs:
            self.connects.append(input)

            input.set(self.value)
            input.owner.eval()

    def set(self, value):
        self.value = value
        
        if not self.level:
            self.level = True
            
            self.update()

        self.level = False

    def delete(self):
        if self.inputed:
            self.deleted.connects.remove(self)

            self.deleted = None
            self.inputed = False
    
    def update(self):       
        for con in self.connects:
            con.set(self.value)

        for con in self.connects:
            try:
                con.owner.eval()
            except:
                continue
                    

    def switch(self):
        self.set(not self.value)

class PrimElement:
    def __init__(self, name, type):
        self.name = name
        self.type = type

        self.inputnames = []
        self.inputs = {}

        self.outputnames = []
        self.outputs = {}

    def add(self, outlet):
        if outlet.out:
            self.outputnames.append(outlet.name)
            self.outputs.update({outlet.name: outlet})

        else:
            self.inputnames.append(outlet.name)
            self.inputs.update({outlet.name: outlet})

    def eval(self):
        return
    
    def update(self):
        for i in self.outputs:
            self.outputs[i].update()

    def connect(self, name, out):
        if not out.inputed:
            self.outputs[name].connect(out)

            out.inputed = True

            out.deleted = self.outputs[name]

            return True
        
        else:
            return False

    def getInput(self, name):
        return self.inputs[name]

    def getOutput(self, name):
        return self.outputs[name]

class Not(PrimElement):
    def __init__(self, name):
        PrimElement.__init__(self, name, 'not')
        
        self.add(outlet(self, 'A', False))
        self.add(outlet(self, 'B', True))

    def eval(self):
        self.outputs['B'].set(NOT(self.inputs['A'].value))

class And(PrimElement):
    def __init__(self, name):
        PrimElement.__init__(self, name, 'and')
        
        self.add(outlet(self, 'A', False))
        self.add(outlet(self, 'B', False))
        self.add(outlet(self, 'C', True))

    def eval(self):
        self.outputs['C'].set(AND(self.inputs['A'].value, self.inputs['B'].value))

class Or(PrimElement):
    def __init__(self, name):
        PrimElement.__init__(self, name, 'or')
        
        self.add(outlet(self, 'A', False))
        self.add(outlet(self, 'B', False))
        self.add(outlet(self, 'C', True))

    def eval(self):
        self.outputs['C'].set(OR(self.inputs['A'].value, self.inputs['B'].value))

class Switch(PrimElement):
    def __init__(self, name):
        PrimElement.__init__(self, name, 'switch')
        
        self.add(outlet(self, 'A', True))

    def eval(self):
        self.outputs['A'].update()

    def switch(self):
        self.outputs['A'].switch()

    def setValue(self, value):
        self.outputs['A'].set(value)

class Bulb(PrimElement):
    def __init__(self, name):
        PrimElement.__init__(self, name, 'bulb')
        
        self.value = False
        
        self.add(outlet(self, 'A', False))

    def eval(self):
        self.value = self.inputs['A'].value


class Constant(PrimElement):
    def __init__(self, name, value):
        PrimElement.__init__(self, name, 'constant')
        self.value = value
        
        self.add(outlet(self, 'A', True))
        self.outputs['A'].value = self.value
    
    def eval(self):
        self.outputs['A'].update()

class Element(PrimElement):
    def __init__(self, name):
        PrimElement.__init__(self, name, name)

        self.gates = []

        self.outlets = []

        self.circuits = []
    
    def add(self, gate):
        if not type(gate) is list:
            gate = [gate]

        for i in gate:
            if i.type == 'bulb':
                self.outputnames.append(i.name)
                self.outputs.update({i.name: i.inputs['A']})

            elif i.type == 'switch':
                self.inputnames.append(i.name)
                i.outputs['A'].value = False
                i.outputs['A'].delete()
                self.inputs.update({i.name: i.outputs['A']})

            elif i.type == 'outlet':
                if i.out:
                    self.outputnames.append(i.name)
                    self.outputs.update({i.name: i})

                else:
                    self.inputnames.append(i.name)
                    self.inputs.update({i.name: i})
                    i.value = False
                    i.delete()

            else:
                self.gates.append(i)
    
    def eval(self):       
        for i in self.inputnames:
            self.inputs[i].update()

    def value(self):
        for o in self.outputs:
            print o, self.outputs[o].value, self.outputs[o].deleted.value

    def inValue(self):
        for i in self.inputnames:
            print i, self.inputs[i].value

gates = {
    'Constant 1': lambda: copy.deepcopy(Constant('', True)),
    'Constant 0': lambda: copy.deepcopy(Constant('', False)),
    'Switch': lambda: copy.deepcopy(Switch('')),
    'Bulb': lambda: copy.deepcopy(Bulb('')),
    'And': lambda: copy.deepcopy(And('')),
    'Or': lambda: copy.deepcopy(Or('')),
    'Not': lambda: copy.deepcopy(Not(''))
}

def loadElements():
    for i in os.listdir('../data/elements'):
        elementFile = open('../data/elements/' + i, 'rt')
        
        addElement(pickle.load(elementFile))
        
        elementFile.close()

def addElement(el):
    gates.update({el.name: lambda: copy.deepcopy(el)})

    if not el.name + '.el' in os.listdir('../data/elements'):
        with open('../data/elements/' + el.name + '.el', 'w') as elementFile:
            pickle.dump(el, elementFile)

        elementFile.close()

def changecon(i, el):
    inputs = i.inputs
    outputs = i.outputs

    newel = el

    for inp in inputs:
        deleted = i.inputs[inp].deleted
        newel.inputs[inp].inputed = i.inputs[inp].inputed

        newel.inputs[inp].deleted = deleted

        if deleted != None:
            deleted.connects.remove(i.inputs[inp])
            deleted.connects.append(newel.inputs[inp])



    for out in outputs:
        connects = i.outputs[out].connects

        newel.outputs[out].connects = connects

        for con in connects:
            con.deleted = newel.outputs[out]
            con.inputed = True

    return newel

def shift(el):
    edited = []
    
    for name in gates:
        gate = gates[name]()

        if 'gates' in dir(gate):
            ed = False

            for index, i in enumerate(gate.gates):
                if i.name == el.name:
                    ed = True

                    newel = changecon(i, gates[el.name]())

                    gate.gates[index] = newel


            for i in gate.circuits:
                if i[3].name == el.name:
                    e = gates[el.name]()
                    i[3] = e

                    for j in gate.outlets:
                        if j[0][0] == i[0] and j[0][1] == i[1] and j[0][2] == i[2]:
                            j[0][3] = e

                        if j[1][0] == i[0] and j[1][1] == i[1] and j[1][2] == i[2]:
                            j[1][3] = e



            if ed and not gate in edited:
                edited.append(gate)

    for i in edited:
        reWrite(i)


def reWrite(el):
    gates.update({el.name: lambda: copy.deepcopy(el)})

    os.remove('../data/elements/' + el.name + '.el')
    with open('../data/elements/' + el.name + '.el', 'w') as elementFile:
        pickle.dump(el, elementFile)

    elementFile.close()

    shift(el)

loadElements()
