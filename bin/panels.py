#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pygame.locals import *
from pygame import *
import pygame

from fillGradient import fill_gradient
from button import button

class panels:
	def __init__(self, workspace):
		self.workspace = workspace
		
		self.panelWidth = self.workspace.window.width
		self.panelHeight = 36
		
		self.panelBg = Surface((self.panelWidth, self.panelHeight))
		
		self.panel1 = Surface((self.panelWidth, self.panelHeight))
		self.panel2 = Surface((self.panelWidth, self.panelHeight))
		
		fill_gradient(self.panelBg, (60, 60, 60), (15, 15, 13))

		
		self.deleteImg = transform.scale(image.load('../data/imgs/delete.png'), (24, 24))

		self.delete = button(12, 6, self.panel1, self.deleteImg, backlight = (60, 60, 0))


		self.renameImg = transform.scale(image.load('../data/imgs/rename.png'), (24, 24))

		self.rename = button(48, 6, self.panel1, self.renameImg, backlight = (60, 60, 0))


		self.getintoImg = transform.scale(image.load('../data/imgs/getinto.png'), (24, 24))

		self.getinto = button(84, 6, self.panel1, self.getintoImg, backlight = (60, 60, 0))


		self.unitinImg = transform.scale(image.load('../data/imgs/create.png'), (24, 24))

		self.unitin = button(120, 6, self.panel1, self.unitinImg, backlight = (60, 60, 0))

		
		self.backImg = transform.scale(image.load('../data/imgs/back.png'), (24, 24))

		self.back = button(156, 6, self.panel1, self.backImg, backlight = (60, 60, 0))

		self.addnewtabImg = transform.scale(image.load('../data/imgs/addtab.png'), (24, 24))

		self.addnewtab = button(192, 6, self.panel1, self.addnewtabImg, backlight = (60, 60, 0))

		self.newprojectImg = transform.scale(image.load('../data/imgs/new.png'), (24, 24))

		self.newproject = button(228, 6, self.panel1, self.newprojectImg, backlight = (60, 60, 0))

		self.loadprojectImg = transform.scale(image.load('../data/imgs/load.png'), (24, 24))

		self.loadproject = button(264, 6, self.panel1, self.loadprojectImg, backlight = (60, 60, 0))

		self.saveprojectImg = transform.scale(image.load('../data/imgs/save.png'), (24, 24))

		self.saveproject = button(300, 6, self.panel1, self.saveprojectImg, backlight = (60, 60, 0))
	
	def resize(self):
		self.panelWidth = self.workspace.window.width
		
		self.panelBg = Surface((self.panelWidth, self.panelHeight))
		
		self.panel1 = Surface((self.panelWidth, self.panelHeight))
		self.panel2 = Surface((self.panelWidth, self.panelHeight))
		
		fill_gradient(self.panelBg, (60, 60, 60), (15, 15, 13))

		self.delete = button(12, 6, self.panel1, self.deleteImg, backlight = (60, 60, 0))

		self.rename = button(48, 6, self.panel1, self.renameImg, backlight = (60, 60, 0))

		self.getinto = button(84, 6, self.panel1, self.getintoImg, backlight = (60, 60, 0))

		self.unitin = button(120, 6, self.panel1, self.unitinImg, backlight = (60, 60, 0))

		self.back = button(156, 6, self.panel1, self.backImg, backlight = (60, 60, 0))

		self.addnewtab = button(192, 6, self.panel1, self.addnewtabImg, backlight = (60, 60, 0))

		self.newproject = button(228, 6, self.panel1, self.newprojectImg, backlight = (60, 60, 0))

		self.loadproject = button(264, 6, self.panel1, self.loadprojectImg, backlight = (60, 60, 0))

		self.saveproject = button(300, 6, self.panel1, self.saveprojectImg, backlight = (60, 60, 0))
	
	def update(self):
		if self.delete.selected:
			self.workspace.info.post('delete')
		
		elif self.rename.selected:
			self.workspace.info.post('rename')

		elif self.getinto.selected:
			self.workspace.info.post('get into gate')

		elif self.unitin.selected:
			self.workspace.info.post('unit in gate')	

		elif self.back.selected:
			self.workspace.info.post('back from gate')

		elif self.addnewtab.selected:
			self.workspace.info.post('add new tab to palette')
		
		elif self.newproject.selected:
			self.workspace.info.post('create new project')	

		elif self.loadproject.selected:
			self.workspace.info.post('load project')

		elif self.saveproject.selected:
			self.workspace.info.post('save this project')

		
		if self.delete.update():
			self.workspace.deleteGate()

		elif self.rename.update():
			self.workspace.renameGate()

		elif self.getinto.update():
			self.workspace.getintoGate()

		elif self.unitin.update():
			self.workspace.unitinGate()

		elif self.back.update():
			self.workspace.backintoGate()

		elif self.addnewtab.update():
			self.workspace.addnewtab()

		elif self.newproject.update():
			self.workspace.newproject()

		elif self.loadproject.update():
			self.workspace.loadproject()

		elif self.saveproject.update():
			self.workspace.saveproject(True)

	def draw(self):
		self.panel1.blit(self.panelBg, (0, 0))
		self.panel2.blit(self.panelBg, (0, 0))

		self.delete.draw()
		self.rename.draw()
		self.unitin.draw()
		self.getinto.draw()	
		self.back.draw()	
		self.addnewtab.draw()
		self.newproject.draw()
		self.loadproject.draw()
		self.saveproject.draw()

		self.workspace.window.screen.blit(self.panel1, (0, 0))
		
		self.workspace.window.screen.blit(self.panel2, (0, self.workspace.window.height - self.panelHeight))
