from pygame.locals import *
from pygame import *
import pygame

import os

class cursor:
    def __init__(self, window):
        mouse.set_visible(False)

        self.window = window

        self.width = 16
        self.height = 20
        
        self.cursors = [pygame.transform.scale(pygame.image.load('../data/imgs/cursors/' + str(i) + '.png'), (self.width, self.height)) for i in range(len(os.listdir('../data/imgs/cursors/')))]
        self.cursor = 0

        self.img = Surface((self.width, self.height)).convert_alpha()
        self.img.fill((0, 0, 0, 0))
        self.img.blit(self.cursors[self.cursor], (0, 0))

        self.x = 0
        self.y = 0

        self.lastx = None
        self.lasty = None

        self.pressed = False

    def changeCursor(self, cursor):
        self.cursor = cursor

        self.img = Surface((self.width, self.height)).convert_alpha()
        self.img.fill((0, 0, 0, 0))
        self.img.blit(self.cursors[self.cursor], (0, 0))
        
    def update(self):
        self.x, self.y = mouse.get_pos()

        if mouse.get_pressed()[0]:
            self.pressed = True

        if self.lasty != self.y:
            self.lasty = self.y

        if self.lastx != self.x:
            self.lastx = self.x

    def draw(self):
        self.window.screen.blit(self.img, (self.x, self.y))