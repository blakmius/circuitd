from pygame.locals import *
from pygame import *
import pygame

import logic

import inputbox

from info import info

from panels import panels

from camera import camera

from gate import gate

from palette import palette

import filebrowser

import answer

collide = lambda x1, x2, y1, y2, w1, w2, h1, h2: x1+w1>x2 and x1<x2+w2 and y1+h1>y2 and y1<y2+h2

def gate2arr(gate):
	return [gate.type, gate.x, gate.y, gate.gate]

def wire2arr(wire):
	return [gate2arr(wire[0]), gate2arr(wire[1]), wire[2], wire[3]]

class workspace:
	def __init__(self, window):
		self.window = window

		self.camera = camera()
		
		self.surface = Surface((self.camera.w, self.camera.h))

		self.lastgates = []
		
		self.gates = []

		self.bgcolor = (219, 219, 219)

		self.gateselected = False
		
		self.offset = 200
		
		self.gridW = 15
		
		self.gridH = 15

		self.pulloutlet = False

		self.lastuoutlets = []

		self.outlets = []

		self.pullin = False

		self.mousepressed = False
		
		self.grid = Surface((self.camera.w + self.gridW, self.camera.h + self.gridH)).convert_alpha()
		
		self.grid.fill((0, 0, 0, 0))

		self.lastcamerax = None
		self.lastcameray = None

		self.intogate = []

		self.info = info(self.window)
		
		for x in range(self.camera.w/self.gridW):
			draw.line(self.grid, (203, 203, 203), (x*self.gridW, 0), (x*self.gridW, self.camera.h + self.gridH))
			for y in range(self.camera.h/self.gridH):
				draw.line(self.grid, (203, 203, 203), (0, y*self.gridH), (self.camera.w + self.gridW, y*self.gridH))

		self.selectframe = False

		self.selectframeX = 0
		self.selectframeY = 0

		self.selectframeW = 1
		self.selectframeH = 1

		self.panels = panels(self)

		self.logic = logic

		self.palette = palette(self.window)

	def drawSelectFrame(self):
		self.selectframesurface = Surface((abs(self.selectframeW), abs(self.selectframeH))).convert_alpha()
		
		self.selectframesurface.fill((0, 121, 219))
		self.selectframesurface.fill((0, 121, 219, 120), (2, 2, abs(self.selectframeW)-4, abs(self.selectframeH)-4))

		x = self.selectframeX if self.selectframeW < 0 else self.selectframeX - self.selectframeW
		y = self.selectframeY if self.selectframeH < 0 else self.selectframeY - self.selectframeH
		
		self.surface.blit(self.selectframesurface, (x - self.camera.x, y - self.camera.y))
				
	def resizeGrid(self, w, h):
		self.gridW = w
		
		self.gridH = h
		
		self.grid = Surface((self.camera.w + self.gridW, self.camera.h + self.gridH)).convert_alpha()
		
		self.grid.fill((0, 0, 0, 0))
		
		for x in range(self.camera.w/self.gridW):
			draw.line(self.grid, (203, 203, 203), (x*self.gridW, 0), (x*self.gridW, self.camera.h + self.gridH))
			for y in range(self.camera.h/self.gridH):
				draw.line(self.grid, (203, 203, 203), (0, y*self.gridH), (self.camera.w + self.gridW, y*self.gridH))

	def resize(self):
		self.palette.resize()

		self.camera.resize(self.window)

		self.surface = Surface((self.camera.w, self.camera.h))

		self.panels.resize()

	def change_offset(self, offset):
		self.offset = offset + 200

	def create_gate(self, type, x = None, y = None, gt = None, rt = False):
		if rt:
			return gate(type, self, x, y, gt, rt)

		if len(self.intogate) == 0:
			self.gates.append(gate(type, self, x, y, gt))

		else:
			if type != 'Switch' and type != 'Bulb':
				agree = True

				for i in self.intogate:
					if type == i[3]:
						agree = False

					d = self.logic.gates[type]()
					if 'gates' in dir(d):
						for j in d.gates:
							if j.type == i[3]:
								agree = False

				if agree:
					self.intogate[-1:][0][0].append(gate(type, self, x, y, gt))

				else:
					self.info.post('can\'t put self in self', 2000, (255, 0, 0), 2)

	def deleteGate(self):
		if len(self.intogate) == 0:
			gates = self.gates
			outlets = self.outlets
		
		else:
			gates = self.intogate[-1:][0][0]
			outlets = self.intogate[-1:][0][1]

		gt = []
		dels = []
			
		for gate in range(len(gates)):			
			if gates[gate].selected:
				if len(self.intogate) > 0:
					if gates[gate].type != 'Switch' and gates[gate].type != 'Bulb':
						gt.append(gates[gate])
				else:
					gt.append(gates[gate])

		if len(gt) == 0:
			self.info.post('select gates to delete', 2000, (255, 255, 0), 1)

		for j in gt:
			for i in outlets:
				if j is i[0] or j is i[1]:
					dels.append(i)

		for i in dels:
			if i in outlets:
				self.deleteOutlet(i[1], i[3])

		for i in gt:
			if i in gates:
				gates.remove(i)

		for i in gates:
			for j in i.gate.inputs:
				i.gate.inputs[j].value = False
		

	def renameGate(self):
		event.get()

		if len(self.intogate) == 0:
			gates = self.gates

		else:
			gates = self.intogate[-1:][0][0]
		
		postinfo = True

		for gate in gates:
			if gate.type == 'Switch' or gate.type == 'Bulb':
				if gate.selected:
					gate.gate.name = inputbox.ask(self.window.screen, 'name:', gate.gate.name)

					gate.rename()

					postinfo = False
					
					break

		if postinfo:
			self.info.post('select switch or bulb to rename', 2000, (255, 255, 0), 1)
			

	def fromarr(self, circuits, wires):
		gates = []

		for i in circuits:
			gates.append(self.create_gate(i[0], i[1], i[2], i[3], True))
		
		outlets = []

		for i in wires:
			gt1 = None
			gt2 = None
			o = i[2]
			inp = i[3]

			for j in gates:
				if j.x == i[0][1] and j.y == i[0][2] and j.gate == i[0][3]:
					gt1 = j

					break

			for j in gates:
				if j.x == i[1][1] and j.y == i[1][2] and j.gate == i[1][3]:
					gt2 = j

					break

			outlets.append([gt1, gt2, o, inp])

		return gates, outlets

	def getintoGate(self):
		lastlenintogate = len(self.intogate)

		if len(self.intogate) == 0:
			self.lastcameray = self.camera.y
			self.lastcamerax = self.camera.x

			gates = self.gates

		else:
			gates = self.intogate[-1:][0][0]

		for gt in gates:
			if gt.selected:
				if 'outlets' in dir(gt.gate):
					gates, outlets = self.fromarr(gt.gate.circuits, gt.gate.outlets)

					self.camera.x, self.camera.y = gates[0].x - self.window.width/2, gates[0].y - self.window.height/2

					self.intogate.append([gates, outlets, [self.camera.x, self.camera.y], gt.gate.name])

					break

		if lastlenintogate <= len(self.intogate):
			self.info.post('select gate to get into him', 2000, (255, 255, 0), 1)

	def backintoGate(self):
		if len(self.intogate) > 0:
			lg = self.logic.gates[self.intogate[-1:][0][3]]()
	
			gts = [gate2arr(i) for i in self.intogate[-1:][0][0]]

			wires = [wire2arr(i) for i in self.intogate[-1:][0][1]]

			el = self.logic.Element(self.intogate[-1:][0][3])
			
			el.add([i.gate for i in self.intogate[-1:][0][0]])

			el.circuits = gts

			el.outlets = wires
			
			el.eval()

			self.logic.reWrite(el)

			gates = []
			
			gates += self.gates
			
			for i in self.intogate:
				gates += i[0]
			
			for i in gates:
				if 'gates' in dir(i.gate):
					ed = False

					for gt in i.gate.gates:
						if gt.name == self.intogate[-1:][0][3]:
							ed = True

					if i.gate.name == self.intogate[-1:][0][3]:
						ed = True
						lg = self.logic.gates[self.intogate[-1:][0][3]]()

					if ed:
						try:
							lg	
						except:
							lg = self.logic.gates[i.gate.name]()
						
						i.gate = lg

						for j in self.outlets:
							if i == j[1] or i == j[0]:
								j[0].gate.outputs[j[0].outputnames[j[2]]].connect(j[1].gate.inputs[j[1].inputnames[j[3]]])
			
			self.intogate.pop()

			if len(self.intogate) > 0:
				self.camera.x, self.camera.y = self.intogate[-1:][0][2]

			else:
				self.camera.x, self.camera.y = self.lastcamerax, self.lastcameray

		else:
			self.info.post('gatemap empty', 2000, (255, 0, 0), 2)

	def unitinGate(self):
		event.get()

		gates = []
		wires = []

		if len(self.intogate) == 0:
			gatess = self.gates
			outletss = self.outlets

		else:
			gatess = self.intogate[-1:][0][0]
			outletss = self.intogate[-1:][0][1]
		
		for i in gatess:
			if i.selected:
				gates.append(i)

		for i in gates:
			for j in outletss:
				if i in j and not j in wires:
					wires.append(j)

		inp = False
		out = False	
		
		for i in gates:
			if i.type == 'Switch' and i.gate.name != '':
				inp = True
				i.gate.outputs['A'].set(False)

			if i.type == 'Bulb' and i.gate.name != '':
				out = True
				i.gate.inputs['A'].set(False)

		if inp and out:
			gts = gates

			gates = [i.gate for i in gates]

			name = inputbox.ask(self.window.screen, 'Element name:', '')

			if name != '':
				if len(self.intogate) == 0:
					for i in gts:
						self.gates.remove(i)

					for i in wires:
						self.outlets.remove(i)

				else:
					self.backintoGate()

				gts = [gate2arr(i) for i in gts]

				wires = [wire2arr(i) for i in wires]

				el = self.logic.Element(name)

				el.add(gates)

				el.circuits = gts

				el.outlets = wires

				el.eval()

				self.logic.addElement(el)

			else:
				self.info.post('name can\'t be \'\'', 2000, (255, 0, 0), 2)

		else:
			self.info.post('gate must have at least one input and output', 2000, (255, 0, 0), 2)

	def deleteOutlet(self, gate, inp):
		if len(self.intogate) == 0:
			gates = self.gates
			outlets = self.outlets
		
		else:
			gates = self.intogate[-1:][0][0]
			outlets = self.intogate[-1:][0][1]

		for i in outlets:
			if i[1] == gate and i[3] == inp:
				i[1].gate.inputs[i[1].inputnames[inp]].delete()

				outlets.remove(i)
				
				break

		for i in gates:
			for j in i.gate.inputs:
				i.gate.inputs[j].value = False

	def addnewtab(self):
		name = inputbox.ask(self.window.screen, 'Tab name:', '')

		if name != '':
			self.palette.addtab(name)

		else:
			self.info.post('name can\'t be \'\'', 2000, (255, 0, 0), 2)

	def saveproject(self, ask = False):
		if not (self.gates == [] and self.outlets == []) or self.gates != self.lastgates or self.outlets != self.lastuoutlets:		
			if ask or answer.ask(self.window, 'save this project'):
				f = {
					'camera': [self.camera.x, self.camera.y,],
					'gates': [gate2arr(i) for i in self.gates],
					'outlets': [wire2arr(i) for i in self.outlets],
					'grid': [self.gridW, self.gridH]}

				if filebrowser.saveas(f):
					self.lastgates = list(self.gates)
					self.lastuoutlets = list(self.outlets)

	def loadproject(self):
		f = filebrowser.readfile()

		if type(f) is dict:
			if all([i in f for i in ['camera', 'gates', 'outlets', 'grid']]):
				self.camera.x, self.camera.y = f['camera']
				self.resizeGrid(f['grid'][0], f['grid'][1])
				self.gates, self.outlets = self.fromarr(f['gates'], f['outlets'])

				self.lastgates = list(self.gates)
				self.lastuoutlets = list(self.outlets)

	def newproject(self):
		self.saveproject()

		self.camera.x, self.camera.y = 0, 0
		self.resizeGrid(15, 15)
		self.gates = []
		self.outlets = []
		self.lastgates = []
		self.lastuoutlets = []
			
			
	def update(self):
		keypress = key.get_pressed()
		
		self.panels.update()
		self.camera.update()

		gate = self.palette.update()

		if type(gate) is int:
			self.change_offset(gate)
			event.post(event.Event(USEREVENT, {'draw': True}))

		if type(gate) is str:
			self.create_gate(gate)
			event.post(event.Event(USEREVENT, {'draw': True}))

		if len(self.intogate) == 0:
			gates = self.gates
			outlets = self.outlets
		
		else:
			gates = self.intogate[-1:][0][0]
			outlets = self.intogate[-1:][0][1]

		for gate in range(len(gates)):			
			if gates[gate].selected:
				if gate < len(gates) - 1:
					a = gates[len(gates) - 1]
					b = gates[gate]
					gates[gate] = a
					gates[len(gates) - 1] = b


			gates[len(gates)-1-gate].update()

		if self.pulloutlet and self.pullin:
			gateout = None
			gatein = None
			
			for gate in range(len(gates)):
				if gates[gate].pullout != None:
					gateout = gate

				if gates[gate].pullin != None:
					gatein = gate

			name = gates[gateout].gate.outputnames[gates[gateout].pullout]

			outlet = gates[gatein].gate.inputs[gates[gatein].inputnames[gates[gatein].pullin]]
			
			if gates[gateout].gate.connect(name, outlet): 	
				outlets.append([gates[gateout], gates[gatein], gates[gateout].pullout, gates[gatein].pullin])	

			for gate in range(len(gates)):
				gates[gate].pullout = None

				gates[gate].pullin = None

			self.pulloutlet = False
			self.pullin = False

		if mouse.get_pressed()[0] and not self.mousepressed and not keypress[K_LCTRL] and not keypress[K_LSHIFT]:
			if not self.pulloutlet and not self.pullin and not self.gateselected:
				self.mousepressed = True

				if mouse.get_pos()[0] > self.offset and 36 < mouse.get_pos()[1] < self.window.height - 36:
					
					self.selectframe = True

					self.selectframeX = mouse.get_pos()[0] + self.camera.x - self.offset

					self.selectframeY = mouse.get_pos()[1] + self.camera.y - 36

		if mouse.get_pressed()[0] and self.selectframe:
			self.selectframeW = self.selectframeX - (mouse.get_pos()[0] + self.camera.x - self.offset)
			self.selectframeH = self.selectframeY - (mouse.get_pos()[1] + self.camera.y - 36)
			
		if not mouse.get_pressed()[0] and self.mousepressed:
			self.mousepressed = False

			if self.selectframe:
				self.selectframe = False

				x = self.selectframeX if self.selectframeW < 0 else self.selectframeX - self.selectframeW
				y = self.selectframeY if self.selectframeH < 0 else self.selectframeY - self.selectframeH

				w = abs(self.selectframeW)
				h = abs(self.selectframeH)

				for i in gates:
					if collide(x, i.x, y, i.y, w, i.w, h, i.h):
						i.selected = True

		
		if not mouse.get_pressed()[0] and (self.pulloutlet and not self.pullin):
			for gate in range(len(gates)):
				gates[gate].pullout = None

				gates[gate].pullin = None

			for i in gates:
				i.pullout = None
				i.pullin = None

			self.pulloutlet = False
			self.pullin = False

	def draw_outlets(self):
		if len(self.intogate) == 0:
			outlets = self.outlets
		
		else:
			outlets = self.intogate[-1:][0][1]

		for i in outlets:
			startpos = [i[0].x - self.camera.x + i[0].w - 3, i[0].y - self.camera.y + i[0].sideIndent+ i[0].connectIndent*i[2]]
			endpos = [i[1].x - self.camera.x + 3, i[1].y - self.camera.y + i[1].sideIndent + i[1].connectIndent*i[3]]

			if i[0].gate.outputs[i[0].outputnames[i[2]]].value:
				color = (0, 121, 219)

			else:
				color = (255, 255, 255)

			draw.line(self.surface, (0, 0, 0), startpos, endpos, 4) 
			draw.line(self.surface, color, startpos, endpos, 2) 
	
	def draw(self):
		self.surface.fill(self.bgcolor)
		
		self.surface.blit(self.grid, (0 - self.camera.x%self.gridW, 0 - self.camera.y%self.gridH))
		
		if len(self.intogate) == 0:
			gates = self.gates
		
		else:
			gates = self.intogate[-1:][0][0]

		for gate in gates:
			gate.draw()

		self.draw_outlets()

		if self.selectframe:
			self.drawSelectFrame()

		self.window.screen.blit(self.surface, (self.offset + 10, 36))

		self.panels.draw()

		self.info.draw()

		self.palette.draw()

