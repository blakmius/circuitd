from pygame.locals import *
from pygame import *
import pygame

from button import button
from slider import slider

from tab import tab

import os
import pickle

percent = lambda max, percent: int(float(max)/100*percent)

strdiv = lambda line, n: [line[i:i+n] for i in range(0, len(line), n)]

collide = lambda x1, x2, y1, y2, w1, w2, h1, h2: x1+w1>x2 and x1<x2+w2 and y1+h1>y2 and y1<y2+h2 

mpx = lambda: mouse.get_pos()[0]
mpy = lambda: mouse.get_pos()[1]

class palette:
    def __init__(self, window):
        self.window = window
        
        self.width = 200
        self.height = self.window.height - 72
    
        self.offset = 0
        self.offsetTo = self.offset

        self.moveSpeed = 10

        self.surface = Surface((self.width, self.height)).convert_alpha()

        self.arrowsWidth = 10
        self.arrowsHeight = 124
    
        self.arrowsBg = Surface((self.arrowsWidth+2, self.height))

        self.arrowsImg2 = transform.scale(image.load('../data/imgs/arrowbutton.png'), (self.arrowsWidth, self.arrowsHeight))
        self.arrowsImg1 = transform.flip(self.arrowsImg2, True, False)

        self.arrows = button(self.width+1, self.height/2 - self.arrowsHeight/2, window.screen, [self.arrowsImg1, self.arrowsImg2], imgnum = 0 if self.offset == 0 - self.width else 1, backlight = (30, 30, 0))

        self.sliderWidth = 15
        self.sliderHeight = self.height
        self.sliderX = self.offset + self.width - self.sliderWidth
        self.sliderY = 36
        self.sliderMaxoffset = 0

        self.slider = slider(window.screen, self.sliderX, self.sliderY, self.sliderWidth, self.sliderHeight, self.sliderMaxoffset)

        self.lastslideroffset = 0

        self.offsetY = 0

        self.mousepressed = False

        self.pannelWidth = 640
        self.pannelHeight = 36
        
        self.offsetreturn = self.offset
        self.pastoffsetreturn = self.offset

        self.tabs = []

        self.elementImg = Surface((50, 50))
        self.elementImg.fill((255, 255, 255), (2, 2, 46, 46))

        self.tabs.append(tab('Elements', [], self, False))
        
        for i in ['Constant 1', 'Constant 0', 'Switch', 'Bulb', 'Not', 'And', 'Or']:
            self.tabs[0].addElement(i)

        for i in os.listdir('../data/elements'):
            name = i.replace('.el', '')
            self.tabs[0].addElement(name)

        self.load()

        self.tabupdated = True
        self.tabdeleted = False

        self.element = None
        
    def resize(self):
        self.width = 200

        self.height = self.window.height - 72

        self.surface = Surface((self.width, self.height)).convert_alpha()

        self.arrowsBg = Surface((self.arrowsWidth+2, self.height))

        self.arrows = button(self.width+1, self.height/2 - self.arrowsHeight/2, self.window.screen, [self.arrowsImg1, self.arrowsImg2], imgnum = 0 if self.offset == 0 - self.width else 1, backlight = (30, 30, 0))

        self.circuitSurface = Surface((self.width - 15, self.height)).convert_alpha()
        self.circuitSurface.fill((0, 0, 0, 0))

        self.sliderHeight = self.height

        self.slider = slider(self.window.screen, self.sliderX, self.sliderY, self.sliderWidth, self.sliderHeight, self.sliderMaxoffset)

        self.arrows.change_coords(x = self.offset + self.width + 1)
        self.slider.change_coords(x = self.offset + self.width - percent(self.width - self.width/4, 10))

    def load(self):
        if 'palette.cfg' in os.listdir('../data'):
            f = open('../data/palette.cfg', 'rt')
            tabs = pickle.load(f)

            self.tabs = []

            for i in tabs:
                delete = False if i[0] == 'Elements' else True
                self.tabs.append(tab(i[0], i[1], self, delete))

    def save(self):
        f = open('../data/palette.cfg', 'w')

        tabs = []

        for i in self.tabs:
            tabs.append([i.name, i.elementnames])

        pickle.dump(tabs, f)

    def changeOffset(self):
        self.offsetTo = self.offset - self.width if not self.offset - self.width < 0 - 360 else 0
    
    def changeOffsetY(self):
        self.offsetY = self.slider.absoffset

    def replaceelement(self, el):
        d = None
        
        for i in self.tabs: 
            h = i.surfaceH if i.open else i.h

            if collide(0, 0, mpy()-36, i.y - int(self.slider.absoffset), 1, 200, 1, h):
                d = i

            i.deleteelement(el)

        d.addElement(el)

        self.save()


    def updateTabs(self):
        els = []

        for i in self.tabs:
            tab = i.update()
            
            if tab == True:
                self.tabupdated = True

                self.updateTabs()

            elif type(tab) is str:
                self.element = tab.replace('\n', ' ')

            els += i.elementnames

        for i in os.listdir('../data/elements'):
            name = i.replace('.el', '')    

            els = [''.join(i.split('\n')) for i in els] 

            if not name in els:
                self.tabs[0].addElement(name)
                self.save()

    def addtab(self, name):
        names = [i.name for i in self.tabs]

        if not name in names:
            self.tabs.append(tab(name, [], self))

            self.tabupdated = True

            self.save()

    def update(self):
        if self.offset != self.offsetTo:
            if self.offset < self.offsetTo:
                self.offset += self.width/self.moveSpeed
                
                if self.offset > self.offsetTo:
                    self.offset = self.offsetTo
            
            else:
                self.offset -= self.width/self.moveSpeed
                if self.offset < self.offsetTo:
                    self.offset = self.offsetTo
            
            self.offsetreturn = self.offset
            self.arrows.change_coords(x = self.offset + self.width + 1)
            self.slider.change_coords(x = self.offset + self.width - percent(self.width - self.width/4, 10))
            
            self.updateTabs()

            event.post(event.Event(USEREVENT, {'draw': True}))

        else:
            if self.arrows.update():
                self.changeOffset()
            
            if self.slider.update():
                self.changeOffsetY()

            if self.lastslideroffset != self.slider.absoffset:
                self.tabupdated = True

                self.lastslideroffset = self.slider.absoffset

            self.updateTabs()
            self.tabdeleted = False

            off = 0

            for i in self.tabs:
                off += i.surfaceH

            if off - self.height > 0:
                self.slider.change_offset(maxoffset = off - self.height)
            
            else:
                self.slider.change_offset(maxoffset = 0)

            self.tabupdated = False

        if self.element != None:
            if not mouse.get_pressed()[0]:
                el = self.element
                
                self.element = None
                
                if mpx() > self.width:
                    return el

                else:
                    self.replaceelement(el)

        if self.pastoffsetreturn != self.offsetreturn:
            self.pastoffsetreturn = self.offsetreturn
            
            return self.offsetreturn

    
    def draw(self):
        self.surface.fill((208, 214, 219, 255))

        for i in self.tabs:
            i.draw()

        self.window.screen.blit(self.surface, (self.offset, self.pannelHeight))
        self.window.screen.blit(self.arrowsBg, (self.offset + self.width, self.pannelHeight))

        self.arrows.draw()
        self.slider.draw()

        if self.element != None:
            self.window.screen.blit(self.elementImg, (mpx() - 48, mpy() - 48))
