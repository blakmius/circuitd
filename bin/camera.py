from pygame.locals import *
from pygame import *
import pygame

collide = lambda x1, x2, y1, y2, w1, w2, h1, h2: x1+w1>x2 and x1<x2+w2 and y1+h1>y2 and y1<y2+h2

class camera:
	w1 = -1440
	h1 = -900
	w2 = 1440
	h2 = 900

	w = 1440 - 10
	h = 900 - 72
	
	x = 0
	y = 0
	
	zoom = 1

	speed = 2
	
	offsetbarrierwidth = 5

	lastmousex = 0
	lastmousey = 0

	def controls(self):
		key = pygame.key.get_pressed()
		mousepos = mouse.get_pos()

		mouseoffsetx = abs(mousepos[0] - self.lastmousex)
		mouseoffsety = abs(mousepos[1] - self.lastmousey)

		if mouse.get_pressed()[2]:
			if mousepos[0] > self.lastmousex:
				self.x -= mouseoffsetx 

			elif mousepos[0] < self.lastmousex:
				self.x += mouseoffsetx 

			if mousepos[1] > self.lastmousey:
				self.y -= mouseoffsety

			elif mousepos[1] < self.lastmousey:
				self.y += mouseoffsety 

		
		self.lastmousex, self.lastmousey = mousepos

		UP = False
		DOWN = False
		LEFT = False
		RIGHT = False

		if key[K_UP]:
			UP = True
	 	elif key[K_DOWN]:
	 		DOWN = True

 		if key[K_LEFT]:
 			LEFT = True
		elif key[K_RIGHT]:
			RIGHT = True

		if UP:
			if self.y - self.speed > self.h1:
				self.y -= self.speed
			else:
				self.y = self.h1
		elif DOWN:
			if self.y + self.speed < self.h2:
				self.y += self.speed
			else:
				self.y = self.h2

		if LEFT:
			if self.x - self.speed > self.w1:
				self.x -= self.speed
			else:
				self.x = self.w1
		elif RIGHT:
			if self.x + self.speed < self.w2:
				self.x += self.speed
			else:
				self.x = self.w2

	def update(self):
		self.controls()

	def resize(self, window):
		self.w = window.width
		self.h = window.height

	def drawPerms(self, ox, oy, ow, oh):
		if collide(self.x, ox, self.y, oy, self.w, ow*self.zoom, self.h, oh*self.zoom):
			return True
		return False
