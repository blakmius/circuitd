from pygame.locals import *
from pygame import *
import pygame

from circuitImage import circuitImage

from pixelCollide import pixelCollide, getMask

import inputbox

mpx = lambda: mouse.get_pos()[0]
mpy = lambda: mouse.get_pos()[1]

def outline(image, color=(0,0,0), threshold=127):
	imgmask = mask.from_surface(image, threshold)
	
	outline_image = Surface(image.get_size()).convert_alpha()
	outline_image.fill((0,0,0,0))
	
	for point in imgmask.outline():
		for i in [(0, 1), (1, 0), (-1, 0), (0, -1)]:
			outline_image.set_at((point[0]+i[0], point[1]+i[1]), color)
	
	return transform.scale(outline_image, (image.get_size()[0] + 2, image.get_size()[1] + 2))
	

class gate:
	def __init__(self, type, workspace, x = None, y = None, gate = None, rt = False):
		self.type = type

		self.workspace = workspace

		self.gate = self.workspace.logic.gates[self.type]() if gate == None else gate

		self.inputs = len(self.gate.inputs)
		self.outputs = len(self.gate.outputs)

		self.inputnames = self.gate.inputnames
		self.outputnames = self.gate.outputnames			
		
		self.x = mpx() + self.workspace.camera.x - self.workspace.offset if x == None else x
		self.y = mpy() + self.workspace.camera.y - 36 if y == None else y

		if (self.type == 'Switch' or self.type == 'Bulb') and not rt:
			if self.gate.name == '':
				self.gate.name = inputbox.ask(self.workspace.window.screen, 'name:', '')
		
		name = self.gate.name if self.gate.name != '' else self.type

		self.image, self.connectIndent = circuitImage(name, 32, self.inputs, self.outputs, self.inputnames, self.outputnames)

		self.w, self.h = self.image.get_size()
		self.outline = outline(self.image, color = (0, 121, 219))
		
		self.mouselastcoords = [0, 0]
		self.pressed = False
		
		self.mousepressed = False
		self.mousepressed1 = False

		self.selected = False
		
		self.mask = getMask(self.image)
		self.rect = Rect(self.x, self.y, self.w, self.h)

		self.sideIndent = self.connectIndent/2

		self.offcirlce = Surface((12, 12)).convert_alpha()

		self.offcirlce.fill((0, 0, 0, 0))

		draw.circle(self.offcirlce, (0, 0, 0), (6, 6), 6)
		draw.circle(self.offcirlce, (255, 255, 255), (6, 6), 4)

		self.oncirlce = Surface((16, 16)).convert_alpha()

		self.oncirlce.fill((0, 0, 0, 0))

		draw.circle(self.oncirlce, (0, 0, 0), (6, 6), 6)
		draw.circle(self.oncirlce, (0, 121, 219), (6, 6), 4)

		self.circlemask = getMask(self.oncirlce)

		self.pullout = None
		self.pullin = None

		self.mouseW = 12
		self.mouseH = 12
		self.mousemask = [[True for x in range(self.mouseW)] for y in range(self.mouseH)]

	def rename(self):
		name = self.gate.name if self.gate.name != '' else self.type

		self.image, self.connectIndent = circuitImage(name, 32, self.inputs, self.outputs, self.inputnames, self.outputnames)
		self.sideIndent = self.connectIndent/2

		self.w, self.h = self.image.get_size()
		self.outline = outline(self.image, color = (0, 121, 219))
		
		self.mask = getMask(self.image)
		self.rect = Rect(self.x, self.y, self.w, self.h)

	def update(self):
		self.rect = Rect(self.x + self.workspace.offset + 10, self.y + 36, self.w, self.h)
		
		rect2 = Rect(mouse.get_pos()[0] + self.workspace.camera.x, mouse.get_pos()[1] + self.workspace.camera.y, 1, 1)

		keypressed = key.get_pressed()

		if pixelCollide(self.rect, rect2, self.mask, [[True]]):
			if self.mousepressed == False:
				if keypressed[K_LCTRL] and mouse.get_pressed()[0]:
					self.selected = not self.selected
					self.mousepressed = True

					if self.selected:
						self.pressed = True
					
					else:
						self.pressed = False
					

				if self.pressed == False and mouse.get_pressed()[0] and self.type == 'Switch':
					self.gate.switch()
					
				if self.mousepressed == False and self.pressed == False and mouse.get_pressed()[0] and self.workspace.gateselected == False:
					self.mousepressed = True
					self.pressed = True

					self.workspace.gateselected = True

					self.selected = True

				
				self.mouselastcoords = mouse.get_pos()

		else:
			if mouse.get_pressed()[0] and not keypressed[K_LCTRL]:
				self.selected = False
				self.mousepressed = False

			else:
				if self.selected and not self.pressed:
					self.pressed = True
					self.mouselastcoords = mouse.get_pos()

		if self.pressed:			
			if mouse.get_pressed()[0] and mouse.get_pos()[0] > self.workspace.offset:
				self.mousepressed = True
				self.pressed = True
				self.selected = True

				dx = self.mouselastcoords[0] - mouse.get_pos()[0]
				dy = self.mouselastcoords[1] - mouse.get_pos()[1]

				if self.mouselastcoords != mouse.get_pos():

					self.mouselastcoords = mouse.get_pos()

				self.x -= dx
				self.y -= dy

			else:
				self.pressed = False

				self.workspace.gateselected = False
		
		rect2 = Rect(mouse.get_pos()[0] - self.workspace.offset - 10 + self.workspace.camera.x - self.mouseW/2, mouse.get_pos()[1] - 36 + self.workspace.camera.y - self.mouseH/2, self.mouseW, self.mouseH)

		if not self.workspace.pulloutlet:
			if mouse.get_pressed()[0] and not self.mousepressed1:
				self.mousepressed1 = True
				
				for i in range(len(self.outputnames)):
					circlerect = Rect(self.x + self.w - 10, self.y + self.sideIndent - 6 + self.connectIndent*i, 12, 12)

					if pixelCollide(circlerect, rect2, self.circlemask, self.mousemask):
						self.workspace.pulloutlet = True
						self.pullout = i

						break

				if not self.workspace.pulloutlet:
					for i in range(len(self.inputnames)):
						circlerect = Rect(self.x - 2, self.y + self.sideIndent - 6 + self.connectIndent*i, 12, 12)
						
						if pixelCollide(circlerect, rect2, self.circlemask, self.mousemask):
							self.workspace.deleteOutlet(self, i)

							break

		if self.workspace.pulloutlet:
			if not mouse.get_pressed()[0] and self.mousepressed1:
				self.mousepressed1 = False
				
				for i in range(len(self.inputnames)):
					circlerect = Rect(self.x - 2, self.y + self.sideIndent - 6 + self.connectIndent*i, 12, 12)
					
					if pixelCollide(circlerect, rect2, self.circlemask, self.mousemask):
						self.workspace.pullin = True
						self.pullin = i

						break
				
		self.gate.eval()
		self.gate.update()

		if mouse.get_pressed()[0]:
			self.mousepressed = True
			self.mousepressed1 = True
		
		if not mouse.get_pressed()[0] and self.mousepressed:
			self.mousepressed = False

		if not mouse.get_pressed()[0] and self.mousepressed1:
			self.mousepressed1 = False
				
	def draw(self):
		if self.workspace.camera.drawPerms(self.x, self.y, self.w, self.h):
			if self.selected:
				self.workspace.surface.blit(self.outline, ((self.x - self.workspace.camera.x - 1, self.y - self.workspace.camera.y - 1)))
			
			self.workspace.surface.blit(self.image, (self.x - self.workspace.camera.x, self.y - self.workspace.camera.y))
			
			for i in range(self.inputs):
				if not self.gate.inputs[self.inputnames[i]].value:
					self.workspace.surface.blit(self.offcirlce, (self.x - self.workspace.camera.x - 2, self.y - self.workspace.camera.y + self.sideIndent - 6 + self.connectIndent*i))
				
				else:
					self.workspace.surface.blit(self.oncirlce, (self.x - self.workspace.camera.x - 2, self.y - self.workspace.camera.y + self.sideIndent - 6 + self.connectIndent*i))
			
			for i in range(self.outputs):
				if not self.gate.outputs[self.outputnames[i]].value:
					self.workspace.surface.blit(self.offcirlce, (self.x - self.workspace.camera.x + self.w - 10, self.y - self.workspace.camera.y + self.sideIndent - 6 + self.connectIndent*i))
				
				else:
					self.workspace.surface.blit(self.oncirlce, (self.x - self.workspace.camera.x + self.w - 10, self.y - self.workspace.camera.y + self.sideIndent - 6 + self.connectIndent*i))

			if self.pullout != None and not self.workspace.pullin:
				startpos = [self.x - self.workspace.camera.x + self.w - 3, self.y - self.workspace.camera.y + self.sideIndent + self.connectIndent*self.pullout]

				if self.gate.outputs[self.outputnames[self.pullout]].value:
					color = (0, 121, 219)

				else:
					color = (255, 255, 255)

				mpx = mouse.get_pos()[0]
				mpy = mouse.get_pos()[1]

				dy = startpos[1] - mpy
				dy += 1 if dy == 0 else 0
				dy = dy/abs(dy)

				draw.line(self.workspace.surface, (0, 0, 0), startpos, (mpx - self.workspace.offset - 8, mpy - 36 - 2*dy), 4)
				draw.line(self.workspace.surface, color, startpos, (mpx - self.workspace.offset - 8, mpy - 36 - 2*dy), 2)
