from pygame.locals import *
from pygame import *
import pygame

from button import button
from fillGradient import fill_gradient

strdiv = lambda line, n: [line[i:i+n] for i in range(0, len(line), n)]

collide = lambda x1, x2, y1, y2, w1, w2, h1, h2: x1+w1>x2 and x1<x2+w2 and y1+h1>y2 and y1<y2+h2 

mpx = lambda: mouse.get_pos()[0]
mpy = lambda: mouse.get_pos()[1]

class tab:
    def __init__(self, name, elements, palette, deleteable = True):
        self.name = name

        self.palette = palette

        self.open = False

        self.deleteable = deleteable
        
        self.w = 190
        self.h = 26

        self.x = 0 - self.palette.offset
        self.y = 0

        self.image = Surface((self.w, self.h))

        fill_gradient(self.image, (60, 60, 60), (15, 15, 13))

        self.fontsize = 18

        self.font = font.Font('../data/fonts/gost.ttf', self.fontsize)

        self.image.blit(self.font.render(self.name, 1, (255, 255, 255)), (4, self.h/2-self.font.size(self.name)[1]/2))

        self.surfaceH = self.h
        
        self.surface = Surface((self.w, self.surfaceH))
        
        self.elements =  []

        self.num = len(self.palette.tabs)

        self.circuitSize = 50        
        self.circuitIntend = 25

        self.buttonImgs = [image.load('../data/imgs/arrowbutton2.png'), transform.flip(image.load('../data/imgs/arrowbutton2.png'), False, True)]
        self.button = button(160, self.h/2 - 7, self.image, self.buttonImgs, imgnum = 1 if self.open else 0, whereuponvars = [self.x, self.y + 36, self.w, self.h], backlight = (30, 30, 0))
        
        self.deletebutton = button(135, self.h/2 - 7, self.image, transform.scale(image.load('../data/imgs/delete.png'), (20, 15)), whereuponvars = [self.x, self.y + 36, self.w, self.h], backlight = (30, 30, 0))

        self.elementImg = Surface((50, 50))
        self.elementImg.fill((255, 255, 255), (2, 2, 46, 46))

        self.mousepressed = False

        self.elementnames = []

        for i in elements:
            self.addElement(i)

    def resize(self):
        d = 0
        
        for i in self.elements:
            d += len(i[0].split('\n'))*(self.fontsize+4)

        if self.open and self.elements != []:
            self.surfaceH = (len(self.elements)/2 + len(self.elements)%2)*self.circuitSize + 2*self.circuitIntend + self.h + d

            self.surface = Surface((self.w, self.surfaceH))

            self.surface.fill((208, 214, 219, 255))

            d = 0

            if len(self.elements) > 1:
                for y in range(len(self.elements)/2):
                    b = 0
                    k = 0

                    el0 = self.elements[y*2]
                    el1 = self.elements[y*2+1]

                    self.surface.blit(el0[1], (20, y*self.circuitSize + (y+1)*self.circuitIntend + self.h + d))
                    
                    for i in range(len(el0[0].split('\n'))):
                        x0 = 45 - self.font.size(el0[0].split('\n')[i])[0]/2
                        y0 = (y+1)*self.circuitSize + (y+1)*self.circuitIntend + self.h + d + i*self.fontsize + i*4

                        self.surface.blit(self.font.render(el0[0].split('\n')[i], 1, (0, 0, 0)), ((x0, y0)))              

                    self.surface.blit(el1[1], (120, y*self.circuitSize + (y+1)*self.circuitIntend + self.h + d))
                    
                    for i in range(len(el1[0].split('\n'))):
                        x1 = 145 - self.font.size(el1[0].split('\n')[i])[0]/2
                        y1 = (y+1)*self.circuitSize + (y+1)*self.circuitIntend + self.h + d + i*self.fontsize + i*4

                        self.surface.blit(self.font.render(el1[0].split('\n')[i], 1, (0, 0, 0)), ((x1, y1)))
                        
                    b = (self.fontsize+4)*len(el0[0].split('\n'))                  
                    
                    k = (self.fontsize+4)*len(el1[0].split('\n'))

                    d += b if b > k else k  

                if len(self.elements)%2 == 1:
                    self.surface.blit(self.elements[-1:][0][1], (20, (y+1)*self.circuitSize + (y+1)*self.circuitIntend + self.h + d))
                    
                    for i in range(len(self.elements[-1:][0][0].split('\n'))):
                        x2 = 45 - self.font.size(self.elements[-1:][0][0].split('\n')[i])[0]/2
                        y2 =(y+2)*self.circuitSize + (y+1)*self.circuitIntend + self.h + d + i*self.fontsize + i*4

                        self.surface.blit(self.font.render(self.elements[-1:][0][0].split('\n')[i], 1, (0, 0, 0)), ((x2, y2)))
            
            else:
                self.surface.blit(self.elements[-1:][0][1], (20, self.circuitIntend + self.h + d))
                
                for i in range(len(self.elements[-1:][0][0].split('\n'))):
                    x3 = 45 - self.font.size(self.elements[-1:][0][0].split('\n')[i])[0]/2
                    y3 = self.circuitSize + self.circuitIntend + self.h + d + i*self.fontsize + i*4

                    self.surface.blit(self.font.render(self.elements[-1:][0][0].split('\n')[i], 1, (0, 0, 0)), ((x3, y3)))
        
        else:
            self.surfaceH = self.h
            self.surface = Surface((self.w, self.surfaceH))
   
    def addElement(self, name, image = None):  
        fullname = str(name)   
        name = name.split()

        for i in range(len(name)):
            if len(name[i]) > 10:
                name[i] = strdiv(name[i], 10)

            else:
                name[i] = [name[i]]

        name = '\n'.join(str(item) for innerlist in name for item in innerlist)

        if image == None:
            image = self.elementImg
        
        else:
            if image.get_size() != (50, 50):
                transform.scale(image, (50, 50))

        self.elementnames.append(fullname)
        
        self.elements.append([name, image, fullname])

        self.resize()

    def deleteelement(self, el):
        if el in self.elementnames:
            self.elementnames.remove(el)

            for i in self.elements:
                if i[2] == el:
                    self.elements.remove(i)

                    break

            self.resize()
            self.palette.tabupdated = True

    def update(self):
        if self.palette.tabupdated:
            self.y = 0
            
            for i in range(self.num):
                if self.palette.tabs[i].open:
                    self.y += self.palette.tabs[i].surfaceH

                else:
                    self.y += self.h

            self.button.change_whereuponvars(y = self.y + 36 - int(self.palette.slider.absoffset))

            if self.deleteable:
                self.deletebutton.change_whereuponvars(y = self.y + 36 - int(self.palette.slider.absoffset))
        
        if self.button.update():
            self.open = not self.open

            self.resize()

            return True

        if self.deleteable and not self.palette.tabdeleted:
            if self.deletebutton.update():
                del self.palette.tabs[self.num]

                for i in self.elementnames:
                    self.palette.tabs[0].addElement(i)

                if len(self.palette.tabs) > self.num:
                    for i in range(self.num, len(self.palette.tabs)):
                        self.palette.tabs[i].num = i

                self.palette.tabdeleted = True

                self.palette.save()

                return True

        if self.open and self.palette.element == None and not self.mousepressed and self.x == 0 and  self.palette.offset == 0:
            if mouse.get_pressed()[0]:
                d = 0

                ost = 1 if len(self.elements)%2 != 0 else 2

                self.mousepressed = True

                mouserect = Rect(mpx(), mpy(), 1, 1)
                if len(self.elements) > 1:
                    for y in range(len(self.elements)/2):
                        r = Rect(20, y*self.circuitSize + (y+1)*self.circuitIntend + self.h + d + self.y - int(self.palette.slider.absoffset) + 36, 50, 50)
                        
                        if r.colliderect(mouserect):
                            return self.elements[y*2][2]
                        
                        r.left = 120
                        
                        if r.colliderect(mouserect):
                            return self.elements[y*2+1][2]                

                        b = (self.fontsize+4)*len(self.elements[y*2][0].split('\n'))                  
                        
                        k = (self.fontsize+4)*len(self.elements[y*2+1][0].split('\n'))

                        d += b if b > k else k 

                    if len(self.elements)%2 == 1:
                        r = Rect(20, (y+1)*self.circuitSize + (y+1)*self.circuitIntend + self.h + d + self.y - int(self.palette.slider.absoffset) + 36, 50, 50)
                        
                        if r.colliderect(mouserect):
                            return self.elements[-1:][0][2] 
                        
                if len(self.elements) == 1:
                    r = Rect(20, self.circuitIntend + self.h + d + self.y - int(self.palette.slider.absoffset) + 36, 50, 50)
                    if r.colliderect(mouserect):
                        return self.elements[-1:][0][2]

        if not mouse.get_pressed()[0] and self.mousepressed:
            self.mousepressed = False
            

    def debugdraw(self):
        d = 0
        if len(self.elements) > 1:
            for y in range(len(self.elements)/2):
                r = Rect(20, y*self.circuitSize + (y+1)*self.circuitIntend + self.h + d + self.y - int(self.palette.slider.absoffset), 50, 50)
                
                self.palette.surface.fill((255, 0, 255), r)
                
                r.left = 120
                
                self.palette.surface.fill((255, 0, 255), r)   

                b = (self.fontsize+4)*len(self.elements[y*2][0].split('\n'))                  
                
                k = (self.fontsize+4)*len(self.elements[y*2+1][0].split('\n'))

                d += b if b > k else k            

            if len(self.elements)%2 == 1:
                r = Rect(20, (y+1)*self.circuitSize + (y+1)*self.circuitIntend + self.h + d + self.y - int(self.palette.slider.absoffset), 50, 50)
                
                self.palette.surface.fill((255, 0, 255), r) 
                
        if len(self.elements) == 1:
            r = Rect(20, self.circuitIntend + self.h + d + self.y - int(self.palette.slider.absoffset), 50, 50)
            self.palette.surface.fill((255, 0, 255), r)

    def draw(self):
        self.button.draw()
        
        if self.deleteable:
            self.deletebutton.draw()

        self.surface.blit(self.image, (0, 0))

        self.palette.surface.blit(self.surface, (0, self.y - int(self.palette.slider.absoffset)))