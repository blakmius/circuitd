from pygame.locals import *
from pygame import *
import pygame

import string

def get_key():
    while 1:
        e = event.wait()
        
        if e.type == QUIT:
            raise SystemExit
        
        if e.type == KEYDOWN:
            if e.key == K_ESCAPE:
                raise SystemExit
            
            else:
                return (e.key, e.unicode)
        
        else:
            pass

def display_box(screen, message, question):
    fontobject = font.Font('../data/fonts/gost.ttf', 18)

    w = 4 + fontobject.size(question)[0]
    
    surface = Surface((100+w+2, 36))
    
    surface1 = Surface((96, 32))

    surface1.fill((255, 255, 255))
    
    if len(message) != 0:
        offset = 96 - fontobject.size(message)[0] if fontobject.size(message)[0] > 96 else 0

        surface1.blit(fontobject.render(message, 1, (0, 0, 0)), (offset, 12))

    surface.blit(surface1, (4 + w, 2))

    surface.blit(fontobject.render(question, 1, (255, 255, 255)), (2, 12))
    
    screen.blit(surface, ((screen.get_width() / 2) - (100+w+2)/2 , (screen.get_height() / 2) - 18))
    
    display.flip()

def ask(screen, question, cString):
    font.init()
    current_string = list(cString)
    
    display_box(screen, string.join(current_string,""), question)
    
    while 1:
        inkey = get_key()

        if inkey[0] == K_BACKSPACE:
            current_string = current_string[0:-1]
        
        elif inkey[0] == K_RETURN:
            break
        
        else:
            current_string += inkey[1]

        display_box(screen, string.join(current_string,""), question)
    
    return string.join(current_string,"")

if __name__ == '__main__':
        main()
