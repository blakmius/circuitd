from Tkinter import Tk
from tkFileDialog import askdirectory, askopenfilename, asksaveasfile

import pickle

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
#Tk.iconbitmap(default='@myicon.xbm')

def getFilePath():
    filename = askopenfilename()
    
    return filename

def getFolderPath():
	filename = askdirectory()

	return filename

def readfile():
	filename = askopenfilename()

	try:
		f = open(filename, 'rt')
	
		d = pickle.load(f)

		return d

	except Exception as e:
		print e
		


def saveas(file):
	try:
		f = asksaveasfile(mode = 'w')

		pickle.dump(file, f)
		f.close()

		return True

	except:
		return False