#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pygame.locals import *
from pygame import *
import pygame

pygame.init()

import time
import os
import sys

import math

from cursor import cursor

get_current_second = lambda: int(time.time())
get_current_milisecond = lambda: int(round(time.time() * 1000))

collide = lambda x1, x2, y1, y2, w1, w2, h1, h2: x1+w1>x2 and x1<x2+w2 and y1+h1>y2 and y1<y2+h2 

distance = lambda x1, y1, x2, y2: int(math.sqrt(abs(x1-x2)**2 + abs(y1-y2)**2))

percent = lambda max, percent: int(float(max)/100*percent)

mpx = lambda: mouse.get_pos()[0]
mpy = lambda: mouse.get_pos()[1]

class window:
    info = display.Info()
    
    width = 640
    height = 480

    minWidth = 640
    minHeight = 480
    
    screen = display.set_mode((width, height), HWSURFACE|DOUBLEBUF|RESIZABLE)
    
    fullscreen = False

    icon = transform.scale(pygame.image.load('../data/imgs/icon.png'), (32, 32))
    display.set_icon(icon)

    display.set_caption('circuitD')
    
    def resize(self, size):
        self.width = size[0]
        self.height = size[1]
        
        if self.width < self.minWidth:
            self.width = self.minWidth

        if self.height < self.minHeight:
            self.height = self.minHeight

        self.screen = pygame.display.set_mode((self.width, self.height), HWSURFACE|DOUBLEBUF|RESIZABLE)

window = window()

from workspace import workspace

workspace = workspace(window)

class fps:
    clock = pygame.time.Clock()
    
    maxfps = 60
    
    def update(self):
        self.clock.tick(self.maxfps)
        

fps = fps()

cursor = cursor(window)

def events():
    e = event.wait()

    if e.type == pygame.QUIT:
        workspace.saveproject()
        raise SystemExit
    
    if e.type == KEYDOWN:
        if e.key == K_F10:
            image.save(window.screen, os.path.join("../"+"screenshots/")+time.strftime("%d_%b_%Y+%H_%M_%S.png", time.localtime()))
        
        elif e.key == K_ESCAPE:
            workspace.saveproject()
            raise SystemExit
        
        else:
            event.post(e)

    if e.type==VIDEORESIZE:
        window.resize(e.size)
        workspace.resize()

    workspace.update()

    cursor.update()

    fps.update()

    draw()

updated = False

def draw():
    global updated

    if not updated:
        updated = True

        window.screen.fill((255, 255, 255))       
        
        workspace.draw() 

        cursor.draw()

        display.update()

def mainloop():    
    global updated

    while True:
        updated = False

        events()

if __name__ == "__main__":
    mainloop()
