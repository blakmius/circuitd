from pygame.locals import *
from pygame import *
import pygame

import time

getSecond = lambda: int(time.time())
getMilisecond = lambda: int(round(time.time() * 1000))

class info:
	def __init__(self, window):
		self.window = window

		self.queue = []

	def post(self, info, time = 1, color = (255, 255, 255), important = 0):
		fontsize = 18 if len(info)*18 < self.window.width else self.window.width/len(info)

		font = pygame.font.Font('../data/fonts/gost.ttf', fontsize) 
		
		add = True
		
		for i in self.queue:
			if i[4] == info:
				add = False

		if add:
			self.queue.append([font.render(info, 1, color), getMilisecond(), time, important, info])

	def draw(self):
		if len(self.queue) != 0:
			info = self.queue[0]

			for i in self.queue:
				if i[3] > 0 and i != info:
					self.queue.remove(info)
					info = i

			
			if getMilisecond() > info[1] + info[2]:
				self.queue = []


			self.window.screen.blit(info[0], (0, self.window.height - info[0].get_size()[1] - 9))
			