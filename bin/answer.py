from pygame.locals import *
from pygame import *
import pygame

import cursor
from button import button

def events(cursor, yes, no):
    e = event.wait()

    if e.type == pygame.QUIT:
        raise SystemExit
    
    if e.type == KEYDOWN:
        if e.key == K_F10:
            image.save(window.screen, os.path.join("../"+"screenshots/")+time.strftime("%d_%b_%Y+%H_%M_%S.png", time.localtime()))
        
        elif e.key == K_ESCAPE:
            raise SystemExit
        
        else:
            event.post(e)

    cursor.update()
    
    if yes.update():
    	return (True)

    if no.update():
    	return (False)

def ask(window, question):	
	lastimg = window.screen.copy()
	w, h = window.width/4, window.height/4
	x, y = window.width/2-w/2, window.height/2-h/2
	
	blackborder = Surface((w, h))

	defaultfontsize = 32

	fontsize = 1

	while True:		
		font = pygame.font.Font('../data/fonts/gost.ttf', fontsize) 

		questionSurface = font.render(question, 1, (255, 255, 255))

		qw, qh = font.size(question)

		if qw > w or fontsize > 48:
			fontsize -= 1
			break

		fontsize += 1

	font = pygame.font.Font('../data/fonts/gost.ttf', fontsize) 

	questionSurface = font.render(question, 1, (255, 255, 255))

	qw, qh = font.size(question)

	bw = w/4
	bh = h-qh if h - qh < 32 else 32

	whereuponvars = [x, y, w, h]

	yes = button(bw/2, h/2-bh/2+10, blackborder, transform.scale(font.render('Yes', 1, (255, 255, 255)), (bw, bh)), backlight = transform.scale(font.render('Yes', 1, (244, 248, 80)), (bw, bh)), whereuponvars = whereuponvars)
	no = button(w-bw*1.5, h/2-bh/2+10, blackborder, transform.scale(font.render('No', 1, (255, 255, 255)), (bw, bh)), backlight = transform.scale(font.render('No', 1, (244, 248, 80)), (bw, bh)), whereuponvars = whereuponvars)

	cur = cursor.cursor(window)
	
	while True:
		btt = events(cur, yes, no)
		if btt != None:
			return btt

		blackborder.fill((37, 116, 180))
		blackborder.fill((0, 0, 0), (2, 2, w-4, h-4))
		
		yes.draw()
		no.draw()

		blackborder.blit(questionSurface, (w/2 - qw/2, 10))
		
		window.screen.blit(lastimg, (0, 0))
		window.screen.blit(blackborder, (x, y))

		cur.draw()

		display.update()